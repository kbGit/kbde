from honcho import environ, command

import os


class HonchoManager:

    def __init__(self, procfile_path="Procfile"):
        self.procfile_path = procfile_path

    def start(self):
        scales_arg = self.get_scales_arg()
        start_args = [
            "start",
            "-f", self.procfile_path,
            "-c", scales_arg,
        ]
        command.main(start_args)

    def get_scales_arg(self):
        scales = self.get_scales()
        scales_list = [f"{key}={value}" for key, value in scales.items()]
        scales_arg = ",".join(scales_list)

        return scales_arg

    def get_scales(self):
        """
        Gets the scales for each process defined in the procfile from the env
        
        If there is a process named "web" in the procfile, then the env var
        to control that process would be "WEB_SCALE"
        """
        procfile = self.get_procfile()
        process_names = procfile.processes.keys()
        env_scales = self.get_env_scales(process_names)

        return env_scales

    def get_env_scales(self, process_names):
        env_scales = {}
        
        for process_name in process_names:
            env_var = f"{process_name.upper()}_SCALE"
            env_value = os.getenv(env_var, 1)
            env_scales[process_name] = env_value

        return env_scales

    def get_procfile(self):
        """
        Get the contents of the procfile as an ordered dict
        """
        if not hasattr(self, "procfile_cache"):

            with open(self.procfile_path) as f:
                self.procfile_cache = environ.parse_procfile(f.read())

        return self.procfile_cache
