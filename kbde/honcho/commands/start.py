from kbde.kbde_cli import command



class Command(command.Command):
    
    def add_arguments(self, parser):
        parser.add_argument(
            "-f",
            "--procfile-path",
            type=str,
            default="Procfile",
        )
    
    def handle(self, procfile_path):
        from kbde.honcho import manager

        honcho_manager = manager.HonchoManager(procfile_path)
        honcho_manager.start()
