from datetime import datetime
import urllib, time


class Scraper:
    default_start_page_url = None
    
    def __init__(self, auth_credentials=None, start_page_url=None):
        self.auth_credentials = auth_credentials
        self.start_page_url = start_page_url

        self.device = None
        self.page_stack = None

    def get_data(self):
        self.device = self.get_device()
        self.page_stack = []

        # Log in
        self.login(self.device, self.auth_credentials)

        # Get the starting url
        page_url = self.get_start_page_url()

        while True:

            # Get the HTML for the current page
            page_html = self.get_page_html(self.device, page_url)
            
            # Save the url to the page_stack for this scraper
            self.page_stack.append(page_url)

            for obj in self.get_data_from_page(page_html):
                yield obj

            # Get the next page url
            page_url = self.get_next_page_url(page_html)

            if page_url is None:
                break

    def get_device(self):
        """
        Returns a device which will be getting data from a web page
        This could be a requests.Session() instance or a Selenium instance
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .get_device()"
        )

    def login(self, device, auth_credentials):
        """
        Authenticate the device with the auth_credentials
        By default this does nothing
        """
        return None

    def get_start_page_url(self):
        """
        Retun the url of the starting page for scraping
        """
        has_start_page = (
            self.start_page_url is not None
            or self.default_start_page_url is not None
        )
        assert has_start_page, (
            f"{self.__class__} must be instantiated with `start_page_url`, "
            f"define .default_start_page_url or override .get_start_page_url()"
        )

        return self.start_page_url or self.default_start_page_url

    def get_page_html(self, device, url):
        """
        Takes a url and returns the HTML from that web page
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .get_page_html()"
        )

    def get_data_from_page(self, page_html):
        """
        Returns a list of dictionaries, created from the given html
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .get_data_from_page()"
        )

    def get_next_page_url(self, page_html):
        """
        Takes page_html and returns the next url
        Returns None if there is no next page
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .get_next_page_url()"
        )

    def get_current_host(self):
        return self.get_host_from_url(self.get_current_url())

    def get_current_url(self):
        if not self.page_stack:
            return None
        else:
            return self.page_stack[-1]

    def get_host_from_url(self, url):
        parsed_url = urllib.parse.urlparse(url)
        return f"{parsed_url.scheme}://{parsed_url.netloc}"


class RequestsDeviceMixin:
    raise_for_status = True

    def get_device(self):
        import requests

        return requests.Session()

    def login(self, device, auth_credentials):
        """
        Implements basic authentication for static sites.
        Override this for more specific login flows, such as cookie auth
        """

        if auth_credentials is None:
            return None

        assert isinstance(auth_credentials, tuple), (
            "auth_credentials must be a tuple"
        )
        assert len(auth_credentials) == 2, (
            "auth_credentials must have a length of 2: (username, password)"
        )

        device.auth = auth_credentials

    def get_page_html(self, device, page_url):
        response = self.get_response(device, page_url)
        return response.text

    def get_response(self, device, page_url):
        response = device.get(page_url)

        if self.get_raise_for_status():
            response.raise_for_status()

        return response

    def get_raise_for_status(self):
        return self.raise_for_status


class HttpClientDeviceMixin:
    """
    Uses a client from `kbde.http_client.http_client`
    """
    http_client_class = None
    
    def get_device(self):
        http_client_class = self.get_http_client_class()
        return http_client_class(**self.get_http_client_kwargs())

    def get_http_client_kwargs(self):
        return {}

    def get_http_client_class(self):
        assert self.http_client_class is not None, (
            f"{self.__class__} must define .http_client_class "
            f"or override .get_http_client_class()"
        )
        return self.http_client_class


class StaticHtmlMixin:
    """
    Scraper which processes static HTML web pages with BeautifulSoup

    This mixin changes the raw html from a response into a BeautifulSoup
    instance, which is then used in the rest of the call to get_data()
    """

    def get_page_html(self, device, url):
        import bs4

        html = super().get_page_html(device, url)

        return bs4.BeautifulSoup(html, features="html.parser")


class SeleniumDeviceMixin:
    selenium_driver = None
    page_load_check_interval = 1
    page_load_timeout = 5

    def get_device(self):
        driver = self.get_selenium_driver()
        return driver(**self.get_selenium_driver_kwargs())

    def get_selenium_driver(self):
        assert self.selenium_driver is not None, (
            f"{self.__class__} must define .selenium_driver"
        )
        return self.selenium_driver

    def get_selenium_driver_kwargs(self):
        return {}

    def get_data(self):
        
        try:

            for obj in super().get_data():
                yield obj

        finally:
            self.kill_browser()

    def kill_browser(self):
        import psutil

        if getattr(self, "device", None) is None:
            return None

        pid = self.device.service.process.pid
        process = psutil.Process(pid)

        for proc in process.children(recursive=True):
            try:
                proc.kill()
            except psutil.NoSuchProcess:
                pass

        process.kill()

    def get_page_html(self, device, url):
        page_load_check_interval = self.get_page_load_check_interval()
        page_load_timeout = self.get_page_load_timeout()

        # Navigate to the url
        device.get(url)

        time_started = datetime.now()

        # Wait for the page to be loaded
        while not self.check_page_loaded(device):
            time_since_start = datetime.now() - time_started

            if time_since_start.seconds > page_load_timeout:
                raise self.PageLoadTimeout

            time.sleep(page_load_check_interval)

        # Return just the html
        return device.page_source

    def get_page_load_check_interval(self):
        return self.page_load_check_interval

    def get_page_load_timeout(self):
        return self.page_load_timeout

    def check_page_loaded(self, device):
        raise NotImplementedError(
            f"{self.__class__} must implement self.check_page_loaded()"
        )

    class SeleniumDeviceException(Exception):
        pass

    class PageLoadTimeout(SeleniumDeviceException):
        pass


class DynamicWebsiteScraper(StaticHtmlMixin, SeleniumDeviceMixin, Scraper):
    pass
