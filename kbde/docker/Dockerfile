FROM ubuntu:focal

SHELL ["/bin/bash", "-c"]

ARG DEBIAN_FRONTEND="noninteractive"
ENV PYTHONUNBUFFERED=1

# Base dependencies
RUN apt update && apt install -y \
    tree parallel gettext vim git wget lsb-release \
    python3-pip build-essential

# Create app dir
WORKDIR /app

# Add Python version file
ADD .python-version /app/

# Install Python version
RUN apt update \
    && PYTHON_VERSION=python`cat .python-version` \
    && apt install -y software-properties-common \
    && add-apt-repository -y ppa:deadsnakes/ppa \
    && apt update \
    && apt install -y $PYTHON_VERSION lib$PYTHON_VERSION-dev $PYTHON_VERSION-distutils $PYTHON_VERSION-venv \
    && $PYTHON_VERSION -m venv /env

# Create entrypoint script
RUN echo $'#!/bin/bash\n\nsource /env/bin/activate\n\nexec "$@"' > /entrypoint.bash

# Custom system dependencies
ADD install.bash /app/
RUN apt update && bash install.bash

# Python dependencies
ADD requirements.txt /app/
RUN source /env/bin/activate && pip install -U pip wheel && pip install -r requirements.txt

# Add everything else
ADD . /app/

# The entrypoint uses the entrypoint script created earlier
ENTRYPOINT ["bash", "/entrypoint.bash"]

# Default command
CMD bash release.bash && kbde_cli.py honcho start -f Procfile-deployed
