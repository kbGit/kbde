from kbde.iot_client import client as iot_client

import time, json


try:

    with open("device.json") as f:
        data = json.load(f)

except FileNotFoundError:
    data = {}


client = iot_client.IotClient(
    host="http://localhost:8010/iot",
    slug="test-device",
    name="Kurtis Device",
    uuid=data.get("uuid"),
)

def component_changed(component, previous_value):
    print(component.server_state, previous_value)

client.register_component(
    "temperature-sensor",
    changed_callback=component_changed,
)

client.set_value("temperature-sensor", "100")


time.sleep(20)


with open("device.json", "w") as f:
    json.dump(client.device.server_state, f)
