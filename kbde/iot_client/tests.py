from kbde.iot_client import iot_client

import unittest, os


class BaseTest(unittest.TestCase):

    def get_client(self, client_class):
        return client_class(**self.get_client_kwargs())

    def get_client_kwargs(self):
        return {
            "host": self.get_host(),
            "api_key": self.get_api_key(),
        }

    def get_host(self):
        host = os.getenv("IOT_TEST_HOST")

        assert host, (
            "Must define the IOT_TEST_HOST env var"
        )

        return host

    def get_api_key(self):
        api_key = os.getenv("IOT_TEST_API_KEY")

        assert api_key, (
            "Must define the IOT_TEST_API_KEY env var"
        )

        return api_key


class Test(BaseTest):
    
    # DeviceCreate

    def test_01_device_create_get(self):
        client = self.get_client(iot_client.DeviceCreate)
        client.get()

    def test_02_device_create_post(self):
        client = self.get_client(iot_client.DeviceCreate)
        response = client.post(
            slug="test-device",
            name="Test Device",
        )
        Test.device_data = response["data"]

    # DeviceUpdate
    
    def test_03_device_update_get(self):
        client = self.get_client(iot_client.DeviceUpdate)
        client.get(uuid=Test.device_data["uuid"])

    def test_04_device_update_post(self):
        client = self.get_client(iot_client.DeviceUpdate)

        Test.device_data["name"] = "Changed Test Name"

        client.post(**self.device_data)

    # DeviceDetail

    def test_05_device_detail_get(self):
        client = self.get_client(iot_client.DeviceDetail)
        client.get(uuid=Test.device_data["uuid"])

    # ComponentCreate

    def test_06_component_create_get(self):
        client = self.get_client(iot_client.ComponentCreate)
        client.get(device_uuid=Test.device_data["uuid"])

    def test_07_component_create_post(self):
        client = self.get_client(iot_client.ComponentCreate)
        response = client.post(
            device_uuid=Test.device_data["uuid"],
            slug="test-component",
            name="Test Component",
            description="Test component description",
        )
        Test.component_data = response["data"]

    # ComponentUpdateValue

    def test_08_component_update_value_get(self):
        client = self.get_client(iot_client.ComponentUpdateValue)
        client.get(
            device_uuid=Test.device_data["uuid"],
            slug=Test.component_data["slug"],
        )

    def test_09_component_update_value_post(self):
        client = self.get_client(iot_client.ComponentUpdateValue)
        client.post(
            device_uuid=Test.device_data["uuid"],
            slug=Test.component_data["slug"],
            value="Updated value",
        )

    # ComponentDetailValue

    def test_10_component_detail_value_get(self):
        client = self.get_client(iot_client.ComponentDetailValue)
        client.get(
            device_uuid=Test.device_data["uuid"],
            slug=Test.component_data["slug"],
        )
