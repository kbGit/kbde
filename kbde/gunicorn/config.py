import os


# WSGI App
wsgi_app = "config.wsgi"

# Bind
port = int(os.getenv("PORT", "5000"))
bind = f"0.0.0.0:{port}"

# Workers
workers = int(os.getenv("WORKER_COUNT", "1"))
