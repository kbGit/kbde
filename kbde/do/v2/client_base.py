from kbde.http_client import client as http_client

import os


class Base(http_client.HttpClient):
    host = "https://api.digitalocean.com/v2"
    headers = {
        "Authorization": "Bearer {api_token}",
    }

    def __init__(self, api_token=os.getenv("DO_API_TOKEN"), **kwargs):
        return super().__init__(api_token=api_token, **kwargs)
