from config.settings_base import *

import os


# App info

APP_NAME = os.getenv("APP_NAME")
assert APP_NAME, "must define APP_NAME environement variable"

APP_HOST = os.getenv("APP_HOST")


# Views

PAGE_TEMPLATE_NAME = "kbde/django/page.html"
HTML_VALIDATE_FORMS = False
DEFAULT_PERMISSION_CLASSES = None


# KBDE Timezone

MIDDLEWARE += [
    "kbde.django.middleware.TimezoneMiddleware",
]


# Whitenoise config

try:
    import whitenoise

    INSTALLED_APPS += [
        "whitenoise.runserver_nostatic",
    ]

    MIDDLEWARE += [
        "whitenoise.middleware.WhiteNoiseMiddleware",
    ]

except ImportError:
    pass


# Database url config

try:
    import dj_database_url

    DATABASES['default'] = dj_database_url.config()

except ImportError:
    pass


# Auth

AUTH_USER_MODEL = "user.User"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"


# Media storage config

AWS_ACCESS_KEY_ID = os.getenv("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = os.getenv("AWS_STORAGE_BUCKET_NAME")
AWS_S3_ENDPOINT_URL = os.getenv("AWS_S3_ENDPOINT_URL")
AWS_QUERYSTRING_AUTH = False

try:
    import storages

    INSTALLED_APPS += [
        "storages",
    ]

    DEFAULT_FILE_STORAGE = "kbde.django.storage_backends.MediaStorage"

except ImportError:
    pass


# Debug

DEBUG = bool(os.getenv("DEBUG"))
TEMPLATE_DEBUG = DEBUG

DEBUG_EMAIL = os.getenv("DEBUG_EMAIL")
DEBUG_PHONE_NUMBER = os.getenv("DEBUG_PHONE_NUMBER")


# Logging

APP_LOG_LEVEL = os.getenv("APP_LOG_LEVEL", "INFO")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
    },
    "formatters": {
        "verbose": {
            "format": ("[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s"),
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "kbde.django.log.AdminEmailHandler",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["console", "mail_admins"],
            "level": APP_LOG_LEVEL,
        },
    },
}


# Email

EMAIL_HOST = os.getenv("EMAIL_HOST", "smtp.sendgrid.net")
EMAIL_PORT = os.getenv("EMAIL_PORT", "587")
EMAIL_USE_TLS = True
EMAIL_HOST_USER = os.getenv("EMAIL_USERNAME", "apikey")
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_PASSWORD")
EMAIL_SUBJECT_PREFIX = f"[{APP_NAME}] "


# Staticfiles config

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')


# Caches

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "cache_table",
    },
}


# Redis Queue

REDIS_URL = os.getenv("REDIS_URL")

try:
    import django_rq

    INSTALLED_APPS += [
        "django_rq",
    ]

    RQ_QUEUES = {
        "default": {
            "USE_REDIS_CACHE": "rq",
        },
    }

    RQ_SYNC = bool(os.getenv("RQ_SYNC"))

except ImportError:
    pass


RQ_JOB_MODEL_QUEUE_NAME = "default"
RQ_JOB_MODEL_FAILURE_TTL = 60 * 60  # 1 hour
CALCULATED_MODEL_QUEUE_NAME = "default"
