from django.db import models
from django.contrib.auth import base_user
from phonenumber_field import modelfields as phonenumber_models
from kbde.django import models as kbde_models


class PhoneNumberUserManager(base_user.BaseUserManager):

    def create_user(self, phone_number, password=None):

        if phone_number is None:
            raise TypeError('Users must have a phone number')

        user = self.model(phone_number=phone_number)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, phone_number, password):

        if password is None:
            raise TypeError('Superusers must have a password')

        user = self.create_user(phone_number, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user

    def get_by_natural_key(self, phone_number):
        return self.get(phone_number=phone_number)


class PhoneNumberUser(kbde_models.User):
    phone_number = phonenumber_models.PhoneNumberField(unique=True)
    username = models.CharField(max_length=kbde_models.MAX_LENGTH_CHAR_FIELD, blank=True)

    objects = PhoneNumberUserManager()

    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = []
    
    class Meta:
        abstract = True

    def __str__(self):
        return f"{self.phone_number}"
