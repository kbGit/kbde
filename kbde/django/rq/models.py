from django.db import models
from django.conf import settings
from kbde.django import models as kbde_models
from kbde.django.rq import utils as rq_utils

import django_rq, rq, uuid, traceback


class RqJobModel(kbde_models.Model):
    RQ_JOB_STATUS_NEW = 0
    RQ_JOB_STATUS_PENDING = 1
    RQ_JOB_STATUS_PROCESSING = 2
    RQ_JOB_STATUS_COMPLETED = 3
    RQ_JOB_STATUS_CANCELED = 4
    RQ_JOB_STATUS_FAILED = -1
    RQ_JOB_STATUS_CHOICES = (
        (RQ_JOB_STATUS_NEW, "New"),
        (RQ_JOB_STATUS_PENDING, "Pending"),
        (RQ_JOB_STATUS_PROCESSING, "Processing"),
        (RQ_JOB_STATUS_COMPLETED, "Completed"),
        (RQ_JOB_STATUS_CANCELED, "Canceled"),
        (RQ_JOB_STATUS_FAILED, "Failed"),
    )
    rq_job_queue_name = settings.RQ_JOB_MODEL_QUEUE_NAME
    rq_job_failure_ttl = settings.RQ_JOB_MODEL_FAILURE_TTL
    rq_job_delete_after_success = False

    rq_job_id = models.UUIDField(null=True, blank=True)
    rq_job_status = models.IntegerField(
        choices=RQ_JOB_STATUS_CHOICES,
        default=RQ_JOB_STATUS_NEW,
    )
    rq_job_exception = models.TextField(blank=True)

    class Meta:
        abstract = True

    @classmethod
    def run_rq_job(cls, *args, **kwargs):
        job = rq.get_current_job()
        instance = cls.get_rq_job_instance(job.id)

        if instance is None:
            return None

        # Set the status to show that the job is processing
        instance.rq_job_status = cls.RQ_JOB_STATUS_PROCESSING
        instance.save(update_fields=["rq_job_status"])

        # Run the job
        try:
            result = instance.rq_job(*args, **kwargs)
        except Exception:
            instance.rq_job_status = cls.RQ_JOB_STATUS_FAILED
            instance.rq_job_exception = traceback.format_exc()
            instance.save(update_fields=["rq_job_status", "rq_job_exception"])
            raise

        # Set the status to show completed
        instance.rq_job_status = cls.RQ_JOB_STATUS_COMPLETED
        instance.save(update_fields=["rq_job_status"])
        instance.rq_job_success()

        if instance.get_rq_job_delete_after_success():
            instance.delete()

        return result

    @classmethod
    def get_rq_job_instance(cls, job_id):

        try:
            return cls.objects.get(rq_job_id=job_id)
        except cls.DoesNotExist:
            return None

    def __str__(self):
        return f"{super().__str__()} [{self.get_rq_job_status_display()}]"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if self.rq_job_status == self.RQ_JOB_STATUS_CANCELED:
            self.stop_rq_job()

        if self.rq_job_status == self.RQ_JOB_STATUS_NEW:
            self.queue_rq_job()

    def delete(self, *args, **kwargs):
        self.stop_rq_job()
        return super().delete(*args, **kwargs)

    def queue_rq_job(self):
        self.stop_rq_job()
        self.rq_job_id = uuid.uuid4()
        self.rq_job_status = self.RQ_JOB_STATUS_PENDING
        self.rq_job_exception = ""
        self.save(
            update_fields=["rq_job_id", "rq_job_status", "rq_job_exception"],
        )
        rq_utils.queue_job(**self.get_queue_job_kwargs())

    def get_queue_job_kwargs(self):
        return {
            "queue_name": self.get_rq_job_queue_name(),
            "job_func": self.__class__.run_rq_job,
            "job_id": str(self.rq_job_id),
            "failure_ttl": self.get_rq_job_failure_ttl(),
        }

    # Helpers

    def stop_rq_job(self):
        job = self.get_rq_job()

        if job is not None:

            try:
                rq.command.send_stop_job_command(job.connection, job.id)
            except rq.exceptions.InvalidJobOperation:
                pass

        return job

    def get_rq_job(self):
        """
        Get the actual job from RQ
        """
        queue = self.get_rq_job_queue()
        return queue.fetch_job(str(self.rq_job_id))

    def get_rq_job_queue(self):
        return django_rq.get_queue(self.get_rq_job_queue_name())

    def get_rq_job_queue_name(self):
        return self.rq_job_queue_name

    def get_rq_job_failure_ttl(self):
        return self.rq_job_failure_ttl

    def get_rq_job_delete_after_success(self):
        return self.rq_job_delete_after_success

    # Override functions

    def rq_job(self, *args, **kwargs):
        raise NotImplementedError(
            f"{self.__class__} must implement .run_rq_job()"
        )

    def rq_job_success(self):
        return None
