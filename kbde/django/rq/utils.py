from django.conf import settings

import django_rq


def queue_job(queue_name, job_func, **kwargs):
    """
    Queues a job to run the given function
    """

    is_async = not settings.RQ_SYNC
    queue = django_rq.get_queue(queue_name, is_async=is_async)

    return queue.enqueue(job_func, **kwargs)


def schedule_job_in(queue_name, job_func, queue_in, **kwargs):
    """
    Schedules a job to run the given function in a given amount of time
    """
    is_async = not settings.RQ_SYNC
    assert is_async, "cannot use the job scheduler synchronously"

    scheduler = django_rq.get_scheduler(queue_name)

    return scheduler.enqueue_in(queue_in, job_func, **kwargs)


def schedule_job_at(queue_name, job_func, queue_at, **kwargs):
    """
    Schedules a job to run the given function a given time
    """
    is_async = not settings.RQ_SYNC
    assert is_async, "cannot use the job scheduler synchronously"

    scheduler = django_rq.get_scheduler(queue_name)

    return scheduler.enqueue_at(queue_at, job_func, **kwargs)
