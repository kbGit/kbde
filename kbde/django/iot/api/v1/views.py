from kbde.django import (
    views as kbde_views,
    permissions as kbde_permissions,
)
from kbde.django.json_views import views as json_views

from kbde.django.iot import models


# Component

class ComponentDetail(json_views.DetailView):
    fields = [
        "uuid",
        "slug",
        "name",
        "description",
        "value",
    ]


class RelatedDeviceMixin(kbde_views.RelatedObjectMixin):
    model = models.Component
    related_model = models.Device
    related_orm_path = "device"
    slug_field = "slug"
    detail_view_class = ComponentDetail
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]


class ComponentDetailValue(RelatedDeviceMixin, json_views.DetailView):
    fields = [
        "value",
    ]


class ComponentList(json_views.ListView):
    detail_view_class = ComponentDetail


class ComponentUpdate(RelatedDeviceMixin, json_views.UpdateView):
    fields = [
        "name",
        "description",
    ]
    detail_view_class = ComponentDetail


class ComponentUpdateValue(RelatedDeviceMixin, json_views.UpdateView):
    fields = [
        "value",
    ]
    detail_view_class = ComponentDetailValue


class ComponentCreate(RelatedDeviceMixin, json_views.CreateView):
    fields = [
        "slug",
        "name",
        "description",
    ]


# Device

class DeviceDetail(json_views.DetailView):
    model = models.Device
    fields = [
        "uuid",
        "slug",
        "name",
        "components",
    ]
    child_views = {
        "components": ComponentList,
    }
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]

    def get_components(self, obj):
        return obj.component_set.all()


class DeviceUpdate(json_views.UpdateView):
    model = models.Device
    fields = [
        "slug",
        "name",
    ]
    detail_view_class = DeviceDetail
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]


class DeviceCreate(json_views.CreateView):
    model = models.Device
    fields = [
        "slug",
        "name",
    ]
    detail_view_class = DeviceDetail
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]


class DeviceDelete(json_views.DeleteView):
    model = models.Device
    fields = [
        "slug",
        "name",
    ]
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]
