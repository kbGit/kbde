from . import views


app_name = "v1"

urlpatterns = [
    views.ComponentDetailValue.get_urls_path("device/<uuid:related_slug>/component/<slug:slug>/value"),
    views.ComponentUpdate.get_urls_path("device/<uuid:related_slug>/component/<slug:slug>/update"),
    views.ComponentUpdateValue.get_urls_path("device/<uuid:related_slug>/component/<slug:slug>/update/value"),
    views.ComponentCreate.get_urls_path("device/<uuid:related_slug>/component/create"),

    views.DeviceDetail.get_urls_path("device/<uuid:slug>"),
    views.DeviceUpdate.get_urls_path("device/<uuid:slug>/update"),
    views.DeviceCreate.get_urls_path("device/create"),
    views.DeviceDelete.get_urls_path("device/<uuid:slug>/delete")
]
