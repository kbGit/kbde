from django.apps import AppConfig


class IotConfig(AppConfig):
    name = 'kbde.django.iot'
    label = 'kbde_django_iot'
    default_auto_field = 'django.db.models.BigAutoField'
