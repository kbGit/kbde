from django.db import models
from kbde.django import models as kbde_models


class Device(kbde_models.Model):
    slug = models.SlugField()
    name = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
        blank=True,
    )

    @classmethod
    def get_user_read_queryset(cls, user):
        return cls.objects.all()

    @classmethod
    def get_user_update_queryset(cls, user):
        return cls.objects.all()

    def __str__(self):
        return self.name or self.slug


class Component(kbde_models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    slug = models.SlugField()
    name = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
        blank=True,
    )
    description = models.TextField(blank=True)
    value = models.TextField(blank=True)

    @classmethod
    def get_user_related_queryset(cls, user, related_model):
        return related_model.objects.all()

    @classmethod
    def get_user_read_queryset(cls, user):
        return cls.objects.all()

    @classmethod
    def get_user_update_queryset(cls, user):
        return cls.objects.all()

    def __str__(self):
        return self.name or self.slug
