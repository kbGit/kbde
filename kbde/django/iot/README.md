# Django IOT

Server-side implementation for authentication and registration of IOT devices.
Facilitates simple, shared-state communication between an IOT device and an
application server.

## Concepts

### Device

The device represents a single IOT device. Each device has:

- `slug`: an identifier for the device which contains only lowercase letters
  and the `-` character.
- `name`: a human-readable name for the device.

Each device has 0 or more `Components` (see below).

### Component

A component represents a single value representing device state. Devices do not
have a default limit to how many components can be added, but applications can
add their own limits programmatically.

Each device has:

- `slug`: an identifier for the device which contains only lowercase letters
  and the `-` character.
- `name`: a human-readable name for the component.
- `description`: text explaining what the component does.
- `value`: The current state, or desired state, of the component.

## Authentication

The following are supported authenitcation methods:

### API key authentication

The application issues an API key to a user, and that API key can be installed
on the device and used to authenticate all requests from the device.

### Slug-based registration

**On roadmap. Not completed**

As devices are manufactured, they are given a permanent, random multi-word
mnemonic, such as `piano-wreck-fault-symbol`. When a device comes online, it
contacts the server and introduces itself with the multi-word nmemonic (as the
device slug). The user then registers the device with the application by
entering the mnemonic into a form. The form looks for an unregistered device
in the database with the given mnemonic, and attaches it to that users account
if found. A key is issued to the device, which is used to authenticate all
requests from the device.

## Device Registration

After authentication, a device will register itself with the server. As part of
this process, the device will create its own record on the server, as well as a
record for each component that is present on the device.

## Component Sync

For each component, the device is responsible for keeping values in sync with
the server. Depending on the device and the application implementation,
component values can be set by the server, the client, or both.
