from django.db import models
from django.conf import settings

from kbde.django import models as kbde_models


class Org(kbde_models.Model):
    name = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
        unique=True,
    )
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, through="OrgUser")

    def __str__(self):
        return self.name

    @classmethod
    def get_user_read_queryset(cls, user):
        assert user.is_authenticated
        return cls.objects.filter(orguser__user=user)


class OrgUser(kbde_models.Model):
    org = models.ForeignKey(Org, on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = ("org", "user",)

    def __str__(self):
        return f"{self.org}: {self.user}"
