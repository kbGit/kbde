from . import views


app_name = "kbde_django_verification"

urlpatterns = [
    views.VerificationLogin.get_urls_path("verification/<uuid:slug>/login"),
]
