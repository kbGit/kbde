from django import shortcuts, urls
from django.conf import settings
from django.contrib import auth
from kbde.django import views as kbde_views

from . import models, forms


class VerificationVerify(kbde_views.UpdateView):
    model = models.Verification
    form_class = forms.VerificationVerify
    prompt_text = "Please enter the verification code that was sent to you"


class VerificationLoginCreateMixin:
    permission_classes = []
    verification_login_url_name = "kbde_django_verification:VerificationLogin"

    def get_success_url(self):
        url = self.get_verification_login_url()
        query_string = self.request.META.get("QUERY_STRING")

        if not query_string:
            return url

        return f"{url}?{query_string}"

    def get_verification_login_url(self):
        return urls.reverse(
            self.verification_login_url_name,
            args=[self.object.uuid]
        )


class VerificationLogin(auth.views.RedirectURLMixin, VerificationVerify):
    """
    Logs a user in if the verification form is submitted successfully
    """
    permission_classes = []

    def form_valid(self, form):
        user = self.object.get_user()
        auth.login(self.request, user)

        return super().form_valid(form)

    def get_default_redirect_url(self):
        return shortcuts.resolve_url(settings.LOGIN_REDIRECT_URL)


# Email

class EmailVerificationCreate(kbde_views.CreateView):
    model = models.EmailVerification
    form_class = forms.EmailVerificationCreate


class EmailVerificationLoginCreate(VerificationLoginCreateMixin,
                                   EmailVerificationCreate):
    prompt_text = "Enter your email to log in"
