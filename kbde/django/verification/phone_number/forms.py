from django import forms
from django.contrib import auth

from . import models


class PhoneNumberVerificationCreate(forms.ModelForm):
    
    class Meta:
        model = models.PhoneNumberVerification
        fields = [
            "phone_number",
        ]

    def clean(self):
        super().clean()

        if self.errors:
            return None

        User = auth.get_user_model()

        # Try to find the user with this email
        try:
            User.objects.get(phone_number=self.cleaned_data["phone_number"])
        except User.DoesNotExist:
            self.add_error("phone_number", "An account with this phone number was not found")
