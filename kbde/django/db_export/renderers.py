import csv


class Renderer:
    file_open_mode = "w"
    file_extension = None

    def render(self, temp_dir, file_name, field_names, data):
        """
        Takes an iterator of dicts and writes them to a file with the given name.
        The given file_name does not include a file extension.
        """
        full_file_name = self.get_full_file_name(temp_dir, file_name)
    
        with open(full_file_name, self.file_open_mode) as open_file:
            self.write_file(open_file, field_names, data)

        return full_file_name

    def write_file(self, open_file, field_names, data):
        raise NotImplementedError(
            f"{self.__class__} must implement .write_file()"
        )
        
    def get_full_file_name(self, temp_dir, file_name):
        file_extension = self.get_file_extension()
        file_name = f"{file_name}{file_extension}"

        return f"{temp_dir}/{file_name}"
        
    def get_file_extension(self):
        assert self.file_extension is not None, (
            f"{self.__class__} must define .file_extension"
        )
        assert self.file_extension.startswith("."), (
            f"{self.__class__}: file extension must start "
            f"with a `.`, for example: `.csv`"
        )
        return self.file_extension


class CsvRenderer(Renderer):
    file_extension = ".csv"

    def write_file(self, open_file, field_names, data):
        writer = csv.DictWriter(open_file, fieldnames=field_names)
        writer.writeheader()

        for obj in data:
            writer.writerow(obj)
