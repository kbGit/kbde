from django.apps import AppConfig


class DbExportConfig(AppConfig):
    name = 'kbde.django.db_export'
    label = 'kbde_django_db_export'
    default_auto_field = 'django.db.models.BigAutoField'
