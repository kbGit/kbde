from django.db import models
from django.core import files
from kbde.django import (
    models as kbde_models,
    views as kbde_views,
)
from kbde.django.bg_process import models as bg_models

from kbde.import_tools import utils as import_utils

from . import renderers

import tempfile


class DbExport(kbde_views.ValueFromObjectMixin, bg_models.BgProcessModel):
    export_model = None
    export_fields = None

    chunk_size = models.IntegerField(default=2000)
    renderer_class = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
    )
    output_file_name = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
        help_text="The output file name, excluding the file extension"
    )
    record_count = models.IntegerField(null=True, blank=True)
    records_complete = models.IntegerField(default=0)
    output_file = models.FileField(
        upload_to=kbde_models.get_upload_to,
        blank=True,
    )
    
    def bg_process(self):
        self.record_count = self.calculate_record_count()
        self.records_complete = 0
        self.save(update_fields=["record_count", "records_complete"])

        data = self.get_data()

        with tempfile.TemporaryDirectory() as temp_dir:
            file_path = self.render_file(temp_dir, data)
            self.save_file(file_path)

    def save_file(self, file_path):
        file_name = file_path.split("/")[-1]
        
        with open(file_path, "rb") as open_file:
            django_file = files.File(open_file)
            self.output_file.save(file_name, django_file, save=True)

    def render_file(self, temp_dir, data):
        renderer = self.get_renderer()
        field_names = self.get_export_fields()
        return renderer.render(
            temp_dir,
            self.output_file_name,
            field_names,
            data,
        )

    def get_renderer(self):
        renderer_class = self.get_renderer_class()
        return renderer_class(**self.get_renderer_kwargs())

    def get_renderer_kwargs(self):
        return {}

    def get_renderer_class(self):
        renderer_class = import_utils.import_from_string(self.renderer_class)

        assert issubclass(renderer_class, renderers.Renderer), (
            f"{renderer_class} is not a subclass of "
            f"`kbde.django.db_export.renderers.Renderer`"
        )

        return renderer_class

    def get_data(self):
        export_fields = self.get_export_fields()
        
        for obj in self.get_queryset().iterator(chunk_size=self.chunk_size):
            yield self.get_object_dict(export_fields, obj)

            self.records_complete += 1

            if self.records_complete % self.chunk_size == 0:
                self.save(update_fields=["records_complete"])

        self.save(update_fields=["records_complete"])

    def get_object_dict(self, export_fields, obj):
        return {
            field: self.get_value_from_object(obj, field)
            for field in export_fields
        }

    def get_export_fields(self):
        assert self.export_fields is not None, (
            f"{self.__class__} must define .export_fields"
        )
        return self.export_fields.copy()

    def calculate_record_count(self):
        return self.get_queryset().count()

    def get_queryset(self):
        return self.export_model.objects.all()
