from django.core.management import base

import time, logging


logger = logging.getLogger("django.management")


class LoopCommand(base.BaseCommand):
    """
    A command which takes an optional `--loop` parameter.
    """
    loop_exception_log_method = logger.error

    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)

        parser.add_argument(
            "--loop",
            type=int,
            help=(
                "By default, this command runs once. Use this parameter to run "
                "this command periodically, waiting this many seconds "
                "between runs."
            ),
        )

        return parser
    
    def handle(self, *args, **options):
        loop = options.get("loop")

        if loop is not None:
            
            while True:

                try:
                    self.handle_loop(*args, **options)
                except Exception as e:
                    self.handle_loop_exception(e, *args, **options)

                    if loop == 0:
                        time.sleep(1)
                
                time.sleep(loop)

        else:
            self.handle_loop(*args, **options)

    def handle_loop(self, *args, **options):
        raise NotImplementedError(
            f"{self.__class__} must implement .handle_loop"
        )

    def handle_loop_exception(self, e, *args, **options):
        log_method = self.get_loop_exception_log_method(*args, **options)
        log_method(
            "%s: %s",
            str(e),
            str(self.__class__),
            exc_info=e,
        )
            
    def get_loop_exception_log_method(self, *args, **options):
        return self.loop_exception_log_method
