from django import http
from django.template import response as template_response
from kbde.django import views as kbde_views


class TemplateResponse(template_response.TemplateResponse):
    
    def render(self):
        """
        Render the response, but if a redirect is raised (i.e. by a partial),
        return an HttpResponseRedirect instead.
        """

        try:
            return super().render()
        except kbde_views.TemplateResponseMixin.Redirect as e:
            return http.HttpResponseRedirect(str(e))
