from django import forms

import json


class Flatpickr(forms.widgets.DateTimeInput):
    template_name = "kbde/django/forms/widgets/Flatpickr.html"

    class Media:
        css = {
            "all": ["https://cdn.jsdelivr.net/npm/flatpickr@4.6.13/dist/flatpickr.min.css"],
        }
        js = (
            "https://cdn.jsdelivr.net/npm/flatpickr@4.6.13/dist/flatpickr.min.js",
        )

    def __init__(self, options=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if options is None:
            options = {}

        self.options = options

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context["widget"]["options"] = json.dumps(self.options)
        return context
