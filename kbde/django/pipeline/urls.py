from . import views


app_name = "kbde_django_pipeline"

urlpatterns = [
    views.CssDocument.get_urls_path("css/<str:sass_hash>/style.css"),
]
