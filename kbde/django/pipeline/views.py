from django import http
from django.utils import cache as cache_utils
from kbde.django import views as kbde_views

from . import common as pipeline_common


class CssDocument(kbde_views.View):
    permission_classes = []
    cache_control_max_age = 60 * 60 * 24 * 30  # 30 days
    
    def get(self, *args, **kwargs):
        pipeline_cache = pipeline_common.get_pipeline_cache()
        sass_hash = kwargs["sass_hash"]
        cache_key = pipeline_common.get_cache_key(sass_hash)
        css_string = pipeline_cache.get(cache_key)

        if css_string is None:
            return http.HttpResponse("", content_type="text/css", status=404)

        response = http.HttpResponse(css_string, content_type="text/css")
        cache_utils.patch_cache_control(
            response,
            max_age=self.cache_control_max_age,
        )

        return response
