from django import urls
from django.conf import settings
from django.core import management
from django.utils import encoding

from . import common as pipeline_common

import sass, hashlib, datetime


class TagExtractor:
    
    def __init__(self, content):
        self.content = content

    def extract(self, start_tag, end_tag):
        tag_content_list = []

        split_content = self.content.split(start_tag)

        if len(split_content) == 1:
            # There are no matching tags
            return self.content, tag_content_list

        content = [split_content[0]]

        for content_part in split_content[1:]:
            split_content_part = content_part.split(end_tag)

            tag_content = split_content_part[0]
            other_content = split_content_part[1:]
            other_content = end_tag.join(other_content)

            tag_content_list.append(tag_content)
            content.append(other_content)

        content = "".join(content)

        unique_tag_content_list = []

        for tag_content in tag_content_list:
            if tag_content in unique_tag_content_list:
                continue

            unique_tag_content_list.append(tag_content)

        return content, unique_tag_content_list


class MiddlewareBase:

    def __init__(self, get_response):
        self.get_response = get_response


class DebugMiddleware(MiddlewareBase):
    """
    Handles the additional collection of staticfiles on each request
    This is controlled by settings
    """
    DEBUG_COLLECTSTATIC = getattr(
        settings,
        "DEBUG_COLLECTSTATIC",
        settings.DEBUG,
    )

    def __call__(self, request):
        response = self.get_response(request)

        if self.DEBUG_COLLECTSTATIC:
            management.call_command("collectstatic", "--no-input")

        return response


class EmbeddedSassMiddleware(MiddlewareBase):
    CACHE_EMBEDDED_SASS = getattr(
        settings,
        "CACHE_EMBEDDED_SASS",
        not settings.DEBUG,
    )
    EMBEDDED_SASS_CACHE_TTL = getattr(
        settings,
        "EMBEDDED_SASS_CACHE_TTL",
        60*60*24,  # 24 hours
    )
    start_style_tag = "<style sass>"
    end_style_tag = "</style>"

    def __call__(self, request):
        response = self.get_response(request)

        if (
            "text/html" not in response.get("content-type", "").lower()
            or not hasattr(response, "content")
            or not response.content
        ):
            return response

        content = encoding.smart_str(response.content)

        extractor = TagExtractor(content)
        content, style_tag_content_list = extractor.extract(
            self.start_style_tag,
            self.end_style_tag,
        )

        if "<html" in content:
            # This is a full-page document
            # Process all sass style tags
            sass_document = self.get_sass(style_tag_content_list)

            if sass_document:
                style_tag = self.get_style_tag(sass_document)
                insertion_point = "</head>"

                content = content.replace(
                    insertion_point,
                    style_tag + insertion_point,
                )

        response.content = str(content)
        response['content-length'] = str(len(response.content))

        return response

    def get_style_tag(self, sass_document):
        sass_hash = self.compile_css(sass_document)
        css_url = self.get_css_url(sass_hash)
        return f'<link rel="stylesheet" href="{css_url}">'

    def compile_css(self, sass_string):
        """
        Take a sass_string, compile the css, and return the sass_hash
        """
        pipeline_cache = pipeline_common.get_pipeline_cache()

        sass_hash = (
            hashlib.sha256(sass_string.encode("latin1")).hexdigest()
        )

        cache_key = pipeline_common.get_cache_key(sass_hash)
        expire_cache_key = pipeline_common.get_expire_cache_key(sass_hash)
        expire_time = pipeline_cache.get(expire_cache_key)

        if not self.CACHE_EMBEDDED_SASS:
            # Always compile embedded sass
            expire_time = None

        # Check to see if the key already exists in the cache
        if (
            expire_time is None
            or expire_time <= datetime.datetime.now()
            or pipeline_cache.get(cache_key) is None
        ):
            # Did not find the cache_key in the cache or the cache is about to
            # expire

            # Compile the css from the sass_string
            css_string = sass.compile(string=sass_string)
            expire_time = (
                datetime.datetime.now()
                + datetime.timedelta(seconds=self.EMBEDDED_SASS_CACHE_TTL - 60)
            )
            
            pipeline_cache.set(
                cache_key,
                css_string,
                self.EMBEDDED_SASS_CACHE_TTL,
            )
            pipeline_cache.set(
                expire_cache_key,
                expire_time,
                self.EMBEDDED_SASS_CACHE_TTL,
            )

        return sass_hash

    def get_css_url(self, sass_hash):
        return urls.reverse(
            "kbde_django_pipeline:CssDocument",
            args=[sass_hash],
        )

    def get_sass(self, style_tag_content_list):
        sass_strings = []

        for style_tag_content in style_tag_content_list:
            if style_tag_content not in sass_strings:
                sass_strings.append(style_tag_content)

        return "\n\n".join(sass_strings)


class EmbeddedSassTagMiddleware(EmbeddedSassMiddleware):

    def get_style_tag(self, sass_document):
        sass_hash = self.compile_css(sass_document)
        pipeline_cache = pipeline_common.get_pipeline_cache()
        cache_key = pipeline_common.get_cache_key(sass_hash)
        css_string = pipeline_cache.get(cache_key)
        return f'<style>{css_string}</style>'


class EmbeddedJavascriptMiddleware(MiddlewareBase):
    start_script_tag = "<script>"
    end_script_tag = "</script>"

    def __call__(self, request):
        response = self.get_response(request)

        if (
            "text/html" not in response.get("content-type", "").lower()
            or not hasattr(response, "content")
            or not response.content
        ):
            return response

        content = encoding.smart_str(response.content)

        extractor = TagExtractor(content)
        content, script_tag_content_list = extractor.extract(
            self.start_script_tag,
            self.end_script_tag,
        )

        script = "\n\n".join(script_tag_content_list)
        script_tag = f"<script>{script}</script>"

        if "<body" in content:
            # Append to the bottom of the body
            insertion_point = "</body>"

            content = content.replace(
                insertion_point,
                script_tag + insertion_point,
            )

        else:
            # Append to the bootom of the document
            content = content + script_tag

        response.content = str(content)
        response['content-length'] = str(len(response.content))

        return response
