from django import forms
from django.core.files import base as files_base

from . import models

import base64, io


class VoicemailCreate(forms.Form):
    message = forms.CharField(
        widget=forms.HiddenInput,
    )

    def __init__(self, instance, *args, **kwargs):
        self.instance = instance or models.Voicemail()
        super().__init__(*args, **kwargs)

    def clean(self):
        super().clean()

        if self.errors:
            return None

        message = self.cleaned_data["message"]

        data = message.split(",")[-1]

        data = base64.b64decode(data)
        data = io.BytesIO(data)

        self.cleaned_data["message"] = files_base.ContentFile(
            data.getvalue(),
            name="voicemail.webm",
        )

    def save(self, *args, **kwargs):
        self.instance.message = self.cleaned_data["message"]
        self.instance.save()
        return self.instance
