from django.apps import AppConfig


class ContactUsConfig(AppConfig):
    name = 'kbde.django.contact_us'
    label = 'kbde_django_contact_us'
    default_auto_field = 'django.db.models.AutoField'
