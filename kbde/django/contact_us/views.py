from django import urls
from kbde.django import views as kbde_views

try:
    from django_recaptcha import fields as recaptcha_fields
    from django_recaptcha import widgets as recaptcha_widgets
except ImportError:
    assert False, (
        "This module requires django-recaptcha: "
        "https://github.com/django-recaptcha/django-recaptcha"
    )

from . import models, forms


class ContactCreate(kbde_views.CreateView):
    template_name = None
    model = models.Contact
    fields = []
    prompt_text = "Contact Us"
    submit_button_text = "Next"
    permission_classes = []
    recaptcha_widget = recaptcha_widgets.ReCaptchaV2Checkbox
    
    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)

        form.fields["captcha"] = self.get_recaptcha_field()

        return form

    def get_recaptcha_field(self):
        widget_kwargs = self.get_recaptcha_widget_kwargs()
        return recaptcha_fields.ReCaptchaField(
            widget=self.recaptcha_widget(**widget_kwargs),
            label="",
        )

    def get_recaptcha_widget_kwargs(self):
        return {}

    def get_success_url(self):
        return urls.reverse(
            "kbde_django_contact_us:ContactDetail",
            args=[self.object.uuid],
        )


class ContactDetail(kbde_views.DetailView):
    model = models.Contact
    permission_classes = []

    def get_queryset(self):
        return super().get_queryset().filter(is_complete=False)


class MessageCreateMixin(kbde_views.RelatedObjectMixin):
    related_model = models.Contact
    related_orm_path = "contact"

    def get_related_queryset(self):
        return super().get_related_queryset().filter(is_complete=False)

    def form_valid(self, form):
        form.instance.is_sent = True

        response = super().form_valid(form)

        form.instance.contact.is_complete = True
        form.instance.contact.save()

        return response
    
    def get_success_url(self):
        return urls.reverse(
            "kbde_django_contact_us:ContactSuccess",
            args=[self.object.contact.uuid],
        )


class MessageCreate(MessageCreateMixin, kbde_views.CreateView):
    template_name = None
    model = models.Message
    fields = [
        "email",
        "message",
    ]
    prompt_text = "Contact Us"
    submit_button_text = "Send"
    permission_classes = []


class VoicemailCreate(MessageCreateMixin, kbde_views.CreateView):
    template_name = None
    model = models.Voicemail
    form_class = forms.VoicemailCreate
    prompt_text = "Record your voicemail"
    submit_button_text = "Send"
    permission_classes = []


class ContactSuccess(kbde_views.DetailView):
    model = models.Contact
    permission_classes = []


class MessageDetail(kbde_views.DetailView):
    model = models.Message
    permission_classes = []


class VoicemailDetail(kbde_views.DetailView):
    model = models.Voicemail
    permission_classes = []
