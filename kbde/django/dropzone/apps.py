from django.apps import AppConfig


class DropzoneConfig(AppConfig):
    name = 'kbde.django.dropzone'
    label = 'kbde_django_dropzone'
    default_auto_field = 'django.db.models.AutoField'
