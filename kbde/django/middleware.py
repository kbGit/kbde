from django.conf import settings
from django.utils import timezone

import zoneinfo


class TimezoneMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        tz = request.COOKIES.get("kb_tz")
        zone = None

        if tz:
            
            try:
                zone = zoneinfo.ZoneInfo(tz)
            except zoneinfo.ZoneInfoNotFoundError:
                pass

        if zone is not None:
            timezone.activate(zone)
        else:
            timezone.deactivate()

        return self.get_response(request)


class MobileAppUserAgentMiddleware:
    """
    Detect if a request came from a mobile application based on the user agent.
    
    Adds a .is_from_mobile_app attribute to the request with a True or False
    value.
    """
    # Matched user agent strings
    user_agent_strings = getattr(
        settings,
        "MOBILE_APP_USER_AGENT_STRINGS",
        None,
    )

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user_agent = request.META.get("HTTP_USER_AGENT")
        user_agent_strings = self.get_user_agent_strings()

        if user_agent:
            user_agent = user_agent.lower()

            for user_agent_string in user_agent_strings:
                
                if user_agent_string.lower() in user_agent:
                    request.is_from_mobile_app = True
                    break

        # If the attribute has not been set by now, then the request was not
        # from a mobile app
        if not hasattr(request, "is_from_mobile_app"):
            request.is_from_mobile_app = False

        return self.get_response(request)
        
    def get_user_agent_strings(self):
        assert self.user_agent_strings is not None, (
            f"{self.__class__} must define .user_agent_strings, or set the "
            f"MOBILE_APP_USER_AGENT_STRINGS setting"
        )
        return self.user_agent_strings.copy()
