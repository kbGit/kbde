from django.conf import settings
from django.core import cache
from django.utils import log

import datetime, hashlib


DEFAULT_CACHE_NAME = "default"
DEFAULT_CACHE_TTL = 60 * 60 * 24 * 7  # 1 week


class AdminEmailHandler(log.AdminEmailHandler):
    """
    This class works exactly like `django.utils.log.AdminEmailHandler`, but it
    only sends emails when the same error hasn't been sent for a while.

    It keeps track of how many similar errors (errors with the same exact
    email subject) have occurred since the last send.

    More detailed information about each error should be in the logs, assuming
    that the project uses another logging config (which the default KBDE
    logging configuration does).
    """

    # How frequently to send emails (in minutes)
    # Default is every hour
    ADMIN_EMAIL_INTERVAL = getattr(settings, "ADMIN_EMAIL_INTERVAL", 60)
    CACHE_NAME = getattr(settings, "ADMIN_EMAIL_CACHE_NAME", DEFAULT_CACHE_NAME)
    CACHE_TTL = getattr(settings, "ADMIN_EMAIL_CACHE_TTL", DEFAULT_CACHE_TTL)
    CACHE_PREFIX = "admin_email_handler"
    
    def send_mail(self, subject, message, *args, **kwargs):
        subject_send = self.get_subject_send(subject)

        if subject_send is None:
            subject_send = {
                "time": None,
                "count_since_send": 0,
            }

        subject_is_new = subject_send["time"] is None

        if subject_is_new:
            needs_send = True
        else:
            minutes_since_last_send = (
                (datetime.datetime.now() - subject_send["time"]).seconds / 60
            )
            needs_send = minutes_since_last_send >= self.ADMIN_EMAIL_INTERVAL

        if needs_send:
            count_since_send = subject_send["count_since_send"]
            message = (
                f"{count_since_send} SIMILAR ERRORS SINCE LAST NOTIFICATION"
                f"\n\n"
                f"{message}"
            )

            super().send_mail(subject, message, *args, **kwargs)

            subject_send["time"] = datetime.datetime.now()
            subject_send["count_since_send"] = 0

        else:
            subject_send["count_since_send"] += 1

        self.set_subject_send(subject, subject_send)

    def get_subject_send(self, subject):
        cache = self.get_cache()
        key = self.get_cache_key(subject)
        return cache.get(key)

    def set_subject_send(self, subject, subject_send):
        cache = self.get_cache()
        key = self.get_cache_key(subject)
        cache.set(key, subject_send, self.CACHE_TTL)

    def get_cache_key(self, subject):
        subject_hash = hashlib.sha256(subject.encode("latin1")).hexdigest()
        return f"{self.CACHE_PREFIX}:{subject_hash}"

    def get_cache(self):
        return cache.caches[self.CACHE_NAME]
