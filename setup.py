from setuptools import setup, find_packages
from setuptools.command import egg_info

from urllib import request
import pathlib, os, zipfile, io, shutil


VERSION = 204
REPO_DIR = pathlib.Path(__file__).resolve().parent


class InstallBootstrapMixin:
    BOOTSTRAP_ARCHIVE_URL = (
        "https://github.com/twbs/bootstrap/archive/refs/tags/v5.3.2.zip"
    )
    BOOTSTRAP_STATIC_PATH = "kbde/django/bootstrap/static"

    def run(self):
        self.install_bootstrap()
        return super().run()

    def install_bootstrap(self):
        static_path = os.path.join(REPO_DIR, self.BOOTSTRAP_STATIC_PATH)
        bootstrap_dest = os.path.join(static_path, "bootstrap")

        if os.path.exists(bootstrap_dest):
            shutil.rmtree(bootstrap_dest)
        
        with request.urlopen(self.BOOTSTRAP_ARCHIVE_URL) as response:
            zip_file = io.BytesIO(response.read())

        with zipfile.ZipFile(zip_file) as open_zip_file:
            open_zip_file.extractall(static_path)

        bootstrap_dir = [
            d for d in os.listdir(static_path)
            if d.startswith("bootstrap")
        ][0]
        
        os.rename(
            os.path.join(static_path, bootstrap_dir),
            bootstrap_dest,
        )


class KbdeEggInfo(InstallBootstrapMixin, egg_info.egg_info):
    pass


setup(
    name="kbde",
    version=VERSION,
    url="https://gitlab.com/kb_git/kbde",
    author="kbuilds, LLC",
    author_email="k@kbuilds.com",
    description="Development environment library. Foundational Python library.",
    packages=find_packages(),
    include_package_data=True,
    scripts=['scripts/kbde_cli.py'],
    cmdclass = {
        "egg_info": KbdeEggInfo,
    },
)
